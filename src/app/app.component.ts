import { Component } from '@angular/core';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  title = 'TakeTrip';
  isCollapsed:boolean = false;
  toggle():void {
    this.isCollapsed = !this.isCollapsed;
  }
}
