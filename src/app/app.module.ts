import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// import { MatAutocompleteModule } from '@angular/material/autocomplete';

import { 
  MatAutocompleteModule, 
  MatFormFieldModule, 
  MatInputModule, 
  MatCheckboxModule, 
  MatOptionModule, 
  MatSelectModule,
  MatSliderModule,
  MatButtonModule
 } from '@angular/material';
// import { MatInputModule } from '@angular/material/input';
// import {MatCheckboxModule} from '@angular/material/checkbox';
// import { MatOptionModule } from '@angular/material/select';


import { ReactiveFormsModule, FormsModule } from '@angular/forms';
// import {MatSelectModule} from '@angular/material/select';
// import {MatInput} from '@angular/material/select';


import { AppRoutingModule } from './app-routing.module';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { GuidesComponent } from './guides/guides.component';
import { ToursComponent } from './tours/tours.component';
import { ContactComponent } from './contact/contact.component';
import { NotFoundComponent } from './not-found/not-found.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutUsComponent,
    GuidesComponent,
    ToursComponent,
    ContactComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    MatOptionModule,
    MatInputModule,
    MatSelectModule,
    MatSliderModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    MatCheckboxModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent },
      { path: 'About-us', component: AboutUsComponent },
      { path: 'Guides', component: GuidesComponent },
      { path: 'Tours', component: ToursComponent },
      { path: 'Contact', component: ContactComponent },
      { path: '**', component: NotFoundComponent }
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
