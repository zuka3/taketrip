import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';

export interface Category {
  value: string;
  viewValue: string;
}

export interface City {
  value: string;
  viewValue: string;
}

export interface Day {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  
  myControl = new FormControl();
  options: string[] = ['Tbilisi', 'Old city', 'Wine'];


  categories: Category[] = [
    {value: 'steak-0', viewValue: 'Active'},
    {value: 'pizza-1', viewValue: 'Pieceful'},
    {value: 'tacos-2', viewValue: 'Adventurous'}
  ];

  cities: City[] = [
    {value: 'steak-0', viewValue: 'Tbilisi'},
    {value: 'pizza-1', viewValue: 'Batumi'},
    {value: 'tacos-2', viewValue: 'Telavi'}
  ];

  days: Day[] = [
    {value: 'steak-0', viewValue: '1 Day'},
    {value: 'pizza-1', viewValue: '2 Days'},
    {value: 'tacos-2', viewValue: '3 Days'}
  ];

  constructor() { }

  ngOnInit() {
  }

}
